import { ConnectedRouter } from 'connected-react-router'
import { Provider } from 'react-redux'
import React from 'react'
import history from 'utils/history'
import App from 'containers/app'
import store from 'store'

import 'static/style/index.scss'

export default function AppIndex() {
	return (
		<Provider store={store}>
			<ConnectedRouter history={history}>
				<App />
			</ConnectedRouter>
		</Provider>
	)
}
