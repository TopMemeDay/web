import React, { memo, useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { withTranslation } from 'react-i18next'
import i18n from 'i18next'

import {
	Burger,
	HeaderContent,
	HeaderProfile,
	HeaderWrapper,
	Lang,
	LangItem,
	LoginBtn,
	Logo,
	Menu,
	MenuItem,
	MenuWrapp,
	SelectLang,
} from './style'
import { Container } from 'components/custom'
import { DropDownArrow, LogoIcon } from 'components/Icons/data'
import { AvatarMini } from 'components/Avatar'
import { createStructuredSelector } from 'reselect'
import { select } from 'containers/Profile/slices'
import { select as auth_select } from 'containers/Auth/slices/selectors'
import { NavLink } from 'react-router-dom'

const Header = (props) => {
	const { t, isAuth } = props

	const currentLang = i18n.language.toUpperCase()

	const changeLanguage = (lng) => {
		i18n.changeLanguage(lng)
	}

	const handleOpenMenu = () => {
		setOpenMenu(!openMenu)
	}

	const [openLang, setOpenLang] = useState(false)
	const [openMenu, setOpenMenu] = useState(false)

	return (
		<HeaderWrapper>
			<Container>
				<HeaderContent>
					<Logo>
						<NavLink to='/home'>
							<LogoIcon width='70px' height='63px' />
						</NavLink>
					</Logo>
					<MenuItem>
						<Lang onClick={() => setOpenLang(!openLang)}>
							{i18n.language.toUpperCase()}
							<SelectLang open={openLang}>
								{currentLang !== 'EN' && (
									<LangItem onClick={() => changeLanguage('en')}>EN</LangItem>
								)}
								{currentLang !== 'UK' && (
									<LangItem onClick={() => changeLanguage('uk')}>UK</LangItem>
								)}
								{currentLang !== 'RU' && (
									<LangItem onClick={() => changeLanguage('ru')}>RU</LangItem>
								)}
							</SelectLang>
							<DropDownArrow open={openLang} />
						</Lang>
					</MenuItem>
					{!isAuth && (
						<HeaderProfile>
							<NavLink to='/auth/signin'>
								<LoginBtn>{t('header.login')}</LoginBtn>
							</NavLink>
						</HeaderProfile>
					)}
				</HeaderContent>
			</Container>
		</HeaderWrapper>
	)
}

Header.propTypes = {
	isAuth: PropTypes.bool,
	user: PropTypes.object,
	t: PropTypes.func,
}

const mapStateToProps = createStructuredSelector({
	user: select.user,
	isAuth: auth_select.isAuth,
})

const withConnect = connect(mapStateToProps, null)

export default compose(memo, withConnect, withTranslation())(Header)
