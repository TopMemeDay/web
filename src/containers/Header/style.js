import styled, { css } from 'styled-components'
import { colors, maxW } from '../../components/variables'

// Header -------------------------

export const HeaderContent = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
`

export const HeaderWrapper = styled.header`
	width: 100%;
	background-color: ${colors.white};
	padding: 10px 50px;
	border-radius: 0 0 10px 10px;
	position: fixed;
	top: 0;
	right: 0;
	left: 0;
	z-index: 4;
	@media ${maxW.tablet} {
		padding: 10px;
	}
`
export const Logo = styled.div`
	font-size: 48px;
	font-weight: bold;
	text-transform: uppercase;
	display: flex;
	@media ${maxW.tablet} {
		svg {
			width: 40px;
			height: 35px;
		}
	}
`

// Menu ---------------------------

export const MenuWrapp = styled.div`
	transition: max-height 0.5s ease-out;
	${(p) =>
		p.open &&
		css`
			max-height: 100% !important;
		`}
	@media ${maxW.tablet} {
		position: fixed;
		/* overflow: auto; */
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		background-size: cover;
		background-color: ${colors.white};
		z-index: 99;
		max-height: 0;
		overflow: hidden;
	}
`

export const Menu = styled.ul`
	display: flex;
	justify-content: space-around;
	align-items: center;
	@media ${maxW.tablet} {
		display: block;
		padding: 30px;
		padding-top: 43px;
	}
`

export const MenuItem = styled.li`
	list-style: none;
	margin: 0 10px;
	&:hover {
		color: ${colors.blue};
		cursor: pointer;
	}
	@media ${maxW.tablet} {
		font-size: 18px;
	}
`
export const HeaderProfile = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
`

// Lanuage ------------------

export const Lang = styled.div`
	position: relative;
	cursor: pointer;
	margin-left: 25px;
	display: flex;
	font-size: 16px;
	@media ${maxW.tablet} {
		margin-left: 0;
		font-size: 14px;
	}
`

export const SelectLang = styled.ul`
	position: absolute;
	top: 25px;
	right: 3px;
	width: 90px;
	border-radius: 4px;
	background: ${colors.white};
	transition: max-height 0.25s ease-out;
	overflow: hidden;
	max-height: 0;
	z-index: 10;
	${(props) =>
		props.open &&
		css`
			max-height: 150px;
		`};
`
export const LangItem = styled.li`
	margin: 10px auto;
	text-align: center;
`

// Login ------------------

export const LoginBtn = styled.div`
	color: ${colors.black};
	&:hover {
		color: ${colors.blue};
		cursor: pointer;
	}
`

//Burger ---------------

export const Burger = styled.span`
	position: absolute;
	height: 3px;
	width: 32px;
	background: #000;
	margin: 0 auto;
	transition: 0.5s;
	right: 5%;
	z-index: 999;
	&::before {
		position: absolute;
		height: 3px;
		width: 22px;
		background: #000;
		margin: 0 auto;
		top: 9px;
		content: '';
		transition: 0.5s;
	}
	&::after {
		position: absolute;
		height: 3px;
		width: 22px;
		background: #000;
		margin: 0 auto;
		bottom: 10px;
		content: '';
		transition: 0.5s;
	}
	${(p) =>
		p.open &&
		css`
			height: 0;
			transition: 0s;
			top: 40%;
			&::before {
				transform: rotate(45deg);
				bottom: 10px;
				transition: 0.5s;
			}
			&::after {
				transform: rotate(-45deg);
				top: 9px;
				transition: 0.5s;
			}
		`}
`
