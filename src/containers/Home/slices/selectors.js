import { createSelector } from 'reselect'

const selectDomain = (state) => state.posts

export const postSelect = {
	posts: createSelector(selectDomain, (sub) => sub.posts),
	hasMore: createSelector(selectDomain, (sub) => sub.hasMore),
	loading: createSelector(selectDomain, (sub) => sub.loading),
}
