import { createSlice, current } from '@reduxjs/toolkit'
import { PostModel } from 'models/post'
import _config from '_config'

export * from './selectors'

const postSlice = createSlice({
	name: 'posts',
	initialState: {
		loading: false,
		posts: [],
		hasMore: false,
	},
	reducers: {
		getPosts: (state) => {
			state.loading = true
		},
		getPostEnd: (state) => {
			state.loading = false
		},
		getPostSuccess: (state, { payload }) => {
			const { posts, totalCount = 0 } = payload
			const newPosts = posts.map((item) => new PostModel(item))
			state.posts = newPosts
			state.hasMore = posts.length < totalCount
			state.loading = false
		},
		getPostError: (state) => {
			state.loading = false
		},
		getPostsLoadStart: (state) => {
			state.loading = true
		},
		getPostsLoadSuccess: (state, { payload }) => {
			const { posts, totalCount = 0 } = payload
			const newPosts = posts.map((item) => new PostModel(item))
			state.posts = [...state.posts, ...newPosts]
			state.hasMore = posts.length < totalCount
			state.loading = false
		},
		getPostsLoadEnd: (state) => {
			state.loading = false
		},
		createStart: (state) => {
			state.loading = true
		},
		createSuccess: (state) => {
			state.loading = false
		},
		createError: (state) => {
			state.loading = false
		},
		onLike: (state) => {
			state.loading = true
		},
		offLike: (state) => {
			state.loading = true
		},
		// likeStart: (state) => {
		// 	state.loading = true
		// },
		likeSuccess: (state) => {
			state.loading = false
		},
		likeError: (state) => {
			state.loading = false
		},
	},
})

export const { actions, reducer } = postSlice
