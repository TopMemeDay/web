import { call, put, takeLatest } from '@redux-saga/core/effects'
import Api from 'utils/api'
import { actions } from './slices'
import Resizer from 'react-image-file-resizer'
import { uploadImage } from 'utils/upload'

const delay = (time) => new Promise((resolve) => setTimeout(resolve, time))

function* getPostsWorker({ payload }) {
	try {
		const { data } = yield call(
			Api.get,
			`/api/posts/popular/?per=${payload?.filter || 'week'}&skip=${
				payload?.skip || 0
			}&limit=${payload?.limit || 10}`
		)
		yield put({
			...actions.getPostSuccess({ posts: data.posts, totalCount: data.count }),
		})
	} catch (e) {
		console.error('ERROR', e)
		yield put({ ...actions.getPostError() })
	}
}

function* infinitiLoad({ payload }) {
	try {
		const { data } = yield call(
			Api.get,
			`/api/posts/popular/?per=${payload?.filter || 'week'}&skip=${
				payload?.skip || 0
			}&limit=${payload?.limit || 10}`
		)
		yield put({
			...actions.getPostsLoadSuccess({
				posts: data.posts,
				totalCount: data.count,
			}),
		})
	} catch (e) {
		console.error('ERROR', e)
		yield put({ ...actions.getPostsLoadEnd() })
	}
}

const resizeFile = (file) =>
	new Promise((resolve) => {
		Resizer.imageFileResizer(
			file,
			'auto',
			500,
			'JPEG',
			100,
			0,
			(uri) => {
				resolve(uri)
			},
			'blob',
			200,
			200
		)
	})

async function contentUpload({ images }) {
	let list = []
	if (!images) {
		return
	}
	try {
		await Promise.all(
			images.map(async (element) => {
				const resized = await resizeFile(element.file)
				const { id } = await uploadImage({ file: resized })
				list.push(id)
			})
		)
		return list
	} catch (e) {
		console.error('Failed upload images', e)
	}
}

function* createPost({ payload }) {
	const { description, images } = payload.payload
	try {
		const upload = yield call(contentUpload, { images })
		const body = {
			description,
			content: upload || [],
		}
		const data = yield call(Api.post, '/api/posts', body)
		yield put({ ...actions.createSuccess() })
		yield call(getPostsWorker)
		data.success && payload.next()
	} catch (e) {
		yield put({ ...actions.createError() })
	}
}

function* likePost({ payload }) {
	const { _id } = payload
	try {
		yield call(Api.put, `/api/posts/like/${_id}`)
		yield call(delay, 2000)
		yield put({ ...actions.likeSuccess() })
	} catch (e) {
		yield put({ ...actions.likeError() })
	}
}

function* dislikePost({ payload }) {
	const { _id } = payload
	try {
		yield call(Api.delete, `/api/posts/like/${_id}`)
		yield call(delay, 2000)
		yield put({ ...actions.likeSuccess() })
	} catch (e) {
		yield put({ ...actions.likeError() })
	}
}

export default function* postSaga() {
	yield takeLatest(actions.getPosts, getPostsWorker)
	yield takeLatest(actions.getPostsLoadStart, infinitiLoad)
	yield takeLatest(actions.createStart, createPost)
	yield takeLatest(actions.onLike, likePost)
	yield takeLatest(actions.offLike, dislikePost)
	yield takeLatest(actions.offLike, dislikePost)
}
