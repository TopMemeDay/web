import Loader from 'components/loader'
import { Post, PostImg, PostWrapper } from 'components/posts/style'
import React, { memo, useEffect, useState } from 'react'
import InfiniteScroll from 'react-infinite-scroller'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { createStructuredSelector } from 'reselect'
import { postSelect, actions } from './slices'
import Modal from 'react-modal'
import { BasicModal } from 'components/modal'
import { select } from 'containers/Auth/slices/selectors'
import { useParams } from 'react-router'
import noImg from 'static/img/noPostImg.png'

const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		padding: '0',
		border: 'none',
		borderRadius: '20px',
	},
	overlay: {
		zIndex: '5',
		backgroundColor: 'rgba(0, 0, 0, 0.55)',
	},
}

function Home({
	user,
	posts,
	isAuth,
	getLoadPosts,
	getPosts,
	hasMore,
	loading,
	...props
}) {
	const [isOpen, setIsOpen] = useState(false)
	const [active, setActive] = useState(null)

	const { filter } = useParams()

	useEffect(() => {
		getPosts({
			filter: filter,
			skip: 0,
			limit: 10,
		})
	}, [filter])

	const handleFetch = () => () => {
		const params = {
			filter: filter || 'week',
			skip: posts?.length || 0,
			limit: 10,
		}
		getLoadPosts(params)
	}

	const openModal = (item) => () => {
		setActive(item)
		setIsOpen(true)
	}

	const closeModal = () => {
		setIsOpen(false)
	}

	return (
		<>
			<Modal isOpen={isOpen} onRequestClose={closeModal} style={customStyles}>
				<BasicModal onClose={closeModal} post={active} />
			</Modal>
			<InfiniteScroll
				className='row justify-content-center'
				pageStart={0}
				loadMore={handleFetch()}
				hasMore={hasMore && loading}
				useWindow={true}
				loader={!isAuth && <Loader key={0} />}
			>
				{posts.length === 0 && !loading && 'Uvaliable Posts'}
				{posts.length > 0 &&
					posts.map((item, _index) => (
						<PostWrapper
							key={_index}
							className='col-12 col-sm-6 col-md-4 col-lg-3'
							onClick={openModal(item)}
						>
							<Post>
								<PostImg src={item?.content[0] || noImg} alt='NOImg' />
							</Post>
						</PostWrapper>
					))}
			</InfiniteScroll>
		</>
	)
}

const mapStateToProps = createStructuredSelector({
	posts: postSelect.posts,
	hasMore: postSelect.hasMore,
	isAuth: select.isAuth,
	loading: postSelect.loading,
})

const withConnect = connect(mapStateToProps, {
	getPosts: actions.getPosts,
	getLoadPosts: actions.getPostsLoadStart,
})

export default compose(withConnect, memo)(Home)
