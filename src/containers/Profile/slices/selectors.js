import { createSelector } from 'reselect'

const selectDomain = (state) => state.profile

export const select = {
	user: createSelector(selectDomain, (sub) => sub.user),
	posts: createSelector(selectDomain, (sub) => sub.posts),
	hasMore: createSelector(selectDomain, (sub) => sub.hasMore),
	loading: createSelector(selectDomain, (sub) => sub.loading),
}
