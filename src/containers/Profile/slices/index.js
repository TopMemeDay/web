import { createSlice } from '@reduxjs/toolkit'
import { PostModel } from 'models/post'
import { UserModel } from 'models/user'
export * from './selectors'

const profileSlice = createSlice({
	name: 'profile',
	initialState: {
		user: new UserModel(),
		posts: [],
		hasMore: false,
		loading: false,
	},
	reducers: {
		getUserStart: (state) => {
			state.loading = true
		},
		getUserSuccess: (state, { payload }) => {
			state.user = new UserModel(payload.user)
			state.loading = false
		},
		getUserError: (state) => {
			state.loading = true
		},
		getPostsStart: (state) => {
			state.loading = true
		},
		getPostsSuccess: (state, { payload }) => {
			state.posts = payload.posts.map((item) => new PostModel(item))
			state.loading = false
			state.hasMore = payload.posts.length < payload.totalCount
		},
		getPostsEnd: (state) => {
			state.loading = false
		},
		uploadStart: (state) => {
			state.loading = true
		},
		uploadSuccess: (state, { payload }) => {
			console.log(
				'Logger user',
				new UserModel({ ...state.user, ...payload.user })
			)
			state.user = new UserModel({ ...state.user, ...payload.user })
			state.loading = false
		},
		uploadEnd: (state) => {
			state.loading = false
		},
	},
})

export const { actions, reducer } = profileSlice
