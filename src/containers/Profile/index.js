import { LargeAvatar } from 'components/Avatar'
import Loader from 'components/loader'
import { Post, PostImg, PostWrapper } from 'components/posts/style'
import React, { memo, useEffect, useState } from 'react'
import { withTranslation } from 'react-i18next'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { createStructuredSelector } from 'reselect'
import { actions, select } from './slices'
import InfiniteScroll from 'react-infinite-scroller'
import {
	DescNik,
	DescNikLine,
	ProfileAvatar,
	ProfileDescription,
	ProfileHeader,
	ProfileHeaderWrapp,
	ProfileWrapp,
	InputHidden,
} from './style'
import { Modal } from '@material-ui/core'
import { BasicModal } from 'components/modal'

import noImg from 'static/img/noPostImg.png'
import { useRef } from 'react'

const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		padding: '0',
		border: 'none',
		borderRadius: '20px',
	},
	overlay: {
		zIndex: '5',
		backgroundColor: 'rgba(0, 0, 0, 0.55)',
	},
}

function Profile(props) {
	const {
		loading,
		user,
		t,
		getUserWorker,
		posts,
		hasMore,
		getUserPostsWorker,
		uploadAva,
	} = props

	let userId = props.match.params.userId
	const [isOpen, setIsOpen] = useState(false)
	const [active, setActive] = useState(null)
	const input_ref = useRef(null)

	const handleFetch = () => () => {
		const params = {
			_id: user.id,
			skip: posts?.length || 0,
			limit: 10,
		}
		getUserPostsWorker(params)
	}
	const openModal = (item) => () => {
		setActive(item)
		setIsOpen(true)
	}
	const closeModal = () => {
		setIsOpen(false)
	}
	const handleClickAva = () => {
		input_ref.current.click()
	}
	const handleChange = (e) => {
		uploadAva({ file: e.target.files[0] })
	}

	useEffect(() => {
		getUserWorker({ id: userId ? `/${userId}` : '' })
	}, [userId])
	useEffect(() => {
		handleFetch()()
	}, [user])

	return (
		<>
			<Modal isOpen={isOpen} onRequestClose={closeModal} style={customStyles}>
				<BasicModal onClose={closeModal} post={active} />
			</Modal>
			<ProfileWrapp>
				<ProfileHeaderWrapp>
					<ProfileHeader>
						<ProfileAvatar>
							<InputHidden
								type='file'
								onChange={handleChange}
								ref={input_ref}
							/>
							<LargeAvatar onClick={handleClickAva} src={user.avatarImage} />
						</ProfileAvatar>
						<ProfileDescription>
							<DescNikLine>
								<DescNik>{user.nickname}</DescNik>
							</DescNikLine>
						</ProfileDescription>
					</ProfileHeader>
				</ProfileHeaderWrapp>
				<InfiniteScroll
					className='row justify-content-center'
					pageStart={0}
					loadMore={handleFetch()}
					hasMore={hasMore && loading}
					useWindow={true}
					loader={<Loader key={0} />}
				>
					{posts.length === 0 && !loading && 'Uvaliable Posts'}
					{posts.length > 0 &&
						posts.map((item, _index) => (
							<PostWrapper
								key={_index}
								className='col-12 col-sm-6 col-md-4 col-lg-3'
								onClick={openModal(item)}
							>
								<Post>
									<PostImg src={item?.content[0] || noImg} alt='NOImg' />
								</Post>
							</PostWrapper>
						))}
				</InfiniteScroll>
			</ProfileWrapp>
		</>
	)
}

const mapStateToProps = createStructuredSelector({
	user: select.user,
	posts: select.posts,
	hasMore: select.hasMore,
	loading: select.loading,
})

const withConnetc = connect(mapStateToProps, {
	getUserWorker: actions.getUserStart,
	getUserPostsWorker: actions.getPostsStart,
	uploadAva: actions.uploadStart,
})

export default compose(withConnetc, memo, withTranslation())(Profile)
