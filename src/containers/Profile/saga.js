import { call, put, takeLatest } from '@redux-saga/core/effects'
import notifier from 'simple-react-notifications'
import Api from 'utils/api'
import { actions } from './slices'
import CustomNotifier from 'components/notification'
import { uploadImage } from 'utils/upload'
import { UploadModel } from 'models/upload'

function* getUserWorker({ payload }) {
	const { id } = payload
	try {
		const { data } = yield call(Api.get, `/api/users${id}`)
		yield put({ ...actions.getUserSuccess({ user: data.user }) })
	} catch (e) {
		const { message } = e.error
		yield put({ ...actions.getUserError() })
		notifier({
			render: ({ id, onClose }) => (
				<CustomNotifier
					key={id}
					onClosePanel={onClose}
					content={message}
					type='error'
				/>
			),
		})
	}
}

function* getUserPostsWorker({ payload }) {
	const { skip, limit = 10, _id } = payload
	try {
		const { data } = yield call(
			Api.get,
			`/api/posts/users?id=${_id}&skip=${skip}&limit=${limit}`
		)
		yield put({ ...actions.getPostsSuccess({ posts: data.posts }) })
	} catch (e) {
		const { message } = e.error
		yield put({ ...actions.getPostsEnd() })
		notifier({
			render: ({ id, onClose }) => (
				<CustomNotifier
					key={id}
					onClosePanel={onClose}
					content={message}
					type='error'
				/>
			),
		})
	}
}

function* uploadAva({ payload }) {
	const { file } = payload
	try {
		const upload_data = yield call(uploadImage, { file })
		const body = {
			avatarImage: new UploadModel(upload_data).id,
		}
		const { data } = yield call(Api.put, `/api/users`, body)
		yield put({ ...actions.uploadSuccess({ user: data.user }) })
	} catch (e) {
		const { message } = e.error
		yield put({ ...actions.uploadEnd() })
		notifier({
			render: ({ id, onClose }) => (
				<CustomNotifier
					key={id}
					onClosePanel={onClose}
					content={message}
					type='error'
				/>
			),
		})
	}
}

export default function* profileSaga() {
	yield takeLatest(actions.getUserStart, getUserWorker)
	yield takeLatest(actions.getPostsStart, getUserPostsWorker)
	yield takeLatest(actions.uploadStart, uploadAva)
}
