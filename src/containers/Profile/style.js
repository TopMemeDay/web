import { colors } from 'components/variables'
import styled from 'styled-components'

export const ProfileWrapp = styled.div`
	max-width: 990px;
	margin: 0 auto;
`

export const ProfileHeaderWrapp = styled.div`
	padding: 30px 20px;
`

export const ProfileHeader = styled.div`
	width: 100%;
	display: flex;
	align-items: center;
	justify-content: space-between;
`

export const ProfileAvatar = styled.div`
	width: 30%;
	text-align: center;
`

export const ProfileDescription = styled.div`
	width: 70%;
`

export const DescNikLine = styled.div`
	width: 100%;
	display: flex;
	justify-content: space-between;
	align-items: center;
`

export const DescNik = styled.span`
	color: ${colors.black};
	font-size: 28px;
	font-weight: 300;
`

export const InputHidden = styled.input`
	position: absolute;
	opacity: 0;
	z-index: -1;
`
