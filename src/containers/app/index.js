import React, { useEffect } from 'react'
import { Switch, Route, Redirect, useLocation } from 'react-router-dom'
import Modal from 'react-modal'

import Auth from 'containers/Auth'
import Header from 'containers/Header'
import { Content, WrapperApp } from './style'
import PrivateRoute from 'components/private-router'
import { Container } from 'components/custom'
import Footer from 'containers/Footer'
import Home from 'containers/Home'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import Profile from 'containers/Profile'

const App = (props) => {
	useEffect(() => {
		Modal.setAppElement('body')
	}, [])
	let location = useLocation()
	const regularExp = new RegExp('/auth/')
	const showSideBar = !regularExp.exec(location?.pathname)
	return (
		<WrapperApp>
			<Header />
			<Content>
				<Container className='container'>
					<Switch>
						<Route exact path='/' render={() => <Redirect to='/home' />} />
						<Route path='/auth' component={Auth} />
						<Route path='/home/:filter?'>
							<Home />
						</Route>
						<PrivateRoute>
							<Route path='/profile/:userId?' component={Profile} />
						</PrivateRoute>
					</Switch>
				</Container>
			</Content>
			{showSideBar && <Footer />}
		</WrapperApp>
	)
}

export default App
