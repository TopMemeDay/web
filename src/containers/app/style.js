import { colors, maxW } from 'components/variables'
import styled from 'styled-components'

export const WrapperApp = styled.div`
	display: flex;
	flex-direction: column;
	height: 100%;
	padding-top: 105px;
	@media ${maxW.tablet} {
		padding-top: 60px;
	}
`

export const Content = styled.main`
	flex: 1 0 auto;
	display: flex;
	justify-content: center;
	align-items: center;
`
