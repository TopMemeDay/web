import { LogoIcon, PlusIcon, UserIcon } from 'components/Icons/data'
import { select } from 'containers/Auth/slices/selectors'
import React, { useState } from 'react'
import { withTranslation } from 'react-i18next'
import { connect } from 'react-redux'
import { NavLink, useLocation } from 'react-router-dom'
import { compose } from 'redux'
import { createStructuredSelector } from 'reselect'
import {
	FootContent,
	FooterItem,
	FooterPlus,
	FooterSeperator,
	ItemImg,
	ItemText,
	PlusWrapp,
	PulsElement,
	Relative,
	WrapperFooter,
} from './style'
import { postSelect } from 'containers/Home/slices'
import Modal from 'react-modal'
import CreatePost from 'components/modal/createPost'
import Sidebar from 'components/sidebar'

const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)',
		padding: '0',
		border: 'none',
		borderRadius: '20px',
	},
	overlay: {
		zIndex: '5',
		backgroundColor: 'rgba(0, 0, 0, 0.55)',
	},
}

function Footer({ isAuth, loading, t, ...props }) {
	const [isOpen, setIsOpen] = useState(false)

	const openModal = () => {
		setIsOpen(true)
	}

	const closeModal = () => {
		setIsOpen(false)
	}

	return (
		<>
			<Modal isOpen={isOpen} onRequestClose={closeModal} style={customStyles}>
				<CreatePost onClose={closeModal} />
			</Modal>
			<WrapperFooter>
				<Sidebar isOpen={isOpen} openCreateModal={openModal} />
			</WrapperFooter>
		</>
	)
}

const mapStateToProps = createStructuredSelector({
	isAuth: select.isAuth,
	loading: postSelect.loading,
})

const withConnect = connect(mapStateToProps, {})

export default compose(withConnect, withTranslation())(Footer)
