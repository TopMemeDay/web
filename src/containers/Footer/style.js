import { colors, maxW } from 'components/variables'
import styled, { css } from 'styled-components'

// Footer Loading animation -------

export const FooterSeperator = styled.div`

	display: ${(p) => (p.loading ? 'block' : 'none')};
	animation: ${(p) => (p.loading ? 'rotate 2s infinite linear' : 'none')};
	width: 290px;
	margin: 0 auto;
	border-radius: 50px 50px 0 0;
  height: 5px;
	position: absolute;
  top: 0;

	background: rgb(48,255,144); /* Old browsers */
	background: -moz-linear-gradient(left,  rgba(48,255,144,1) 0%, rgba(237,45,237,1) 25%, rgba(201,152,38,1) 50%, rgba(48,255,230,1) 75%, rgba(48,255,144,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(48,255,144,1)), color-stop(25%,rgba(237,45,237,1)), color-stop(50%,rgba(201,152,38,1)), color-stop(75%,rgba(48,255,230,1)), color-stop(100%,rgba(48,255,144,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(left,  rgba(48,255,144,1) 0%,rgba(237,45,237,1) 25%,rgba(201,152,38,1) 50%,rgba(48,255,230,1) 75%,rgba(48,255,144,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(left,  rgba(48,255,144,1) 0%,rgba(237,45,237,1) 25%,rgba(201,152,38,1) 50%,rgba(48,255,230,1) 75%,rgba(48,255,144,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(left,  rgba(48,255,144,1) 0%,rgba(237,45,237,1) 25%,rgba(201,152,38,1) 50%,rgba(48,255,230,1) 75%,rgba(48,255,144,1) 100%); /* IE10+ */
	background: linear-gradient(to right,  rgba(48,255,144,1) 0%,rgba(237,45,237,1) 25%,rgba(201,152,38,1) 50%,rgba(48,255,230,1) 75%,rgba(48,255,144,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#30ff90', endColorstr='#30ff90',GradientType=1 ); /* IE6-9 */

	@keyframes rotate {
  from {
    background-position: -3000px;
  }
  to { 
    background-position: 0px;
  }
`

// Footer styling -----------------

export const WrapperFooter = styled.footer`
	width: 100%;
	z-index: 4;
	position: fixed;
	bottom: 0;
	left: 0;
	right: 0;
	background: transparent;
	height: auto;
	display: flex;
	align-items: flex-end;
	justify-content: center;
	border-radius: 10px 10px 0 0;
	flex-wrap: wrap;
}
`

export const FootContent = styled.div`
	width: 100%;
	display: flex;
	justify-content: space-evenly;
	align-items: center;
`

// Footer item -------------------

export const FooterItem = styled.div`
	max-width: 200px;
	display: flex;
	align-items: center;
	cursor: pointer;
`

export const ItemImg = styled.div`
	height: 20px;
	margin-right: 10px;
`

export const ItemText = styled.span`
	color: ${colors.black};
	@media ${maxW.mobileXL} {
		display: none;
	}
`

// Footer Plus styling ----------

export const FooterPlus = styled.div`
	border-radius: 50%;
	width: 70px;
	height: 70px;
	position: absolute;
	left: 50%;
	top: -32px;
	background: ${colors.lightGrey};
	cursor: pointer;
	transform: translate(-50%, 0);
	@media ${maxW.tablet} {
		width: 50px;
		height: 50px;
		top: -24px;
	}
`

export const Relative = styled.div`
	position: relative;
	height: 70px;
	@media ${maxW.tablet} {
		height: 50px;
	}
`

export const PlusWrapp = styled.div`
	position: absolute;
	top: 50%;
	left: 50%;
	z-index: 10;
	width: 20px;
	height: 20px;
	transform: translate(-50%, -50%);
`

export const PulsElement = styled.div`
	border-radius: 50%;
	box-shadow: 0 0 0 0 rgba(0, 0, 0, 1);
	height: 25px;
	width: 25px;
	transform: scale(1);
	transform: translate(-50%, -50%) !important;
	animation: pulse-black 2s infinite;
	position: absolute;
	top: 50%;
	left: 50%;

	@media ${maxW.tablet} {
		animation: pulse-black-mobile 2s infinite;
	}

	@keyframes pulse-black {
		0% {
			transform: scale(0);
			box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.7);
		}

		70% {
			transform: scale(0.5);
			box-shadow: 0 0 0 23px rgba(0, 0, 0, 0);
		}

		100% {
			transform: scale(0);
			box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);
		}
	}

	@keyframes pulse-black-mobile {
		0% {
			transform: scale(0);
			box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.7);
		}

		70% {
			transform: scale(0.5);
			box-shadow: 0 0 0 13px rgba(0, 0, 0, 0);
		}

		100% {
			transform: scale(0);
			box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);
		}
	}
`
