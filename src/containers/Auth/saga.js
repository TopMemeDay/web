import { call, put, takeLatest } from '@redux-saga/core/effects'
import { push } from 'connected-react-router'
import i18n from 'i18n'
import notifier from 'simple-react-notifications'
import Api from 'utils/api'
import { setToken } from 'utils/auth'
import { actions } from './slices'
import CustomNotifier from 'components/notification'

function* loginWorker({ payload }) {
	const { email, password } = payload
	try {
		const body = {
			email,
			password,
		}
		const { data } = yield call(Api.post, '/api/auth/login', body)
		setToken(data.token)
		yield put({ ...actions.loginSuccess({ id: data.id }) })
		yield put(push('/home'))
	} catch (e) {
		const { message } = e.error
		yield put({ ...actions.loginError() })
		notifier({
			render: ({ id, onClose }) => (
				<CustomNotifier
					key={id}
					onClosePanel={onClose}
					content={message}
					type='error'
				/>
			),
		})
	}
}

function* registerWorker({ payload }) {
	const { nickname, email, password } = payload
	try {
		const body = {
			nickname,
			email,
			password,
		}
		yield call(Api.post, '/api/auth/register', body)
		yield put({ ...actions.registerSuccess() })
		yield call(loginWorker, { payload })
	} catch (e) {
		console.error(e)
		const { message } = e.error
		yield put({ ...actions.registerError() })
		notifier({
			render: ({ id, onClose }) => (
				<CustomNotifier
					key={id}
					onClosePanel={onClose}
					content={message}
					type='error'
				/>
			),
		})
	}
}

export default function* authSaga() {
	yield takeLatest(actions.loginStart, loginWorker)
	yield takeLatest(actions.registerStart, registerWorker)
}
