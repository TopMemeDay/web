import React, { memo } from 'react'
import PropTypes from 'prop-types'
import { TextMiddle, TitleLarge } from 'components/styled'
import { connect, useDispatch } from 'react-redux'
import { withTranslation } from 'react-i18next'
import { compose } from 'redux'
import { createStructuredSelector } from 'reselect'
import { actions } from '../slices'
import { select } from '../slices/selectors'
import { NavLink, Redirect } from 'react-router-dom'
import { Form, Formik } from 'formik'
import * as Yup from 'yup'
import Input from 'components/form/input'
import CustomButton from 'components/form/Button'

function Login(props) {
	const { isAuth, t, loginWorker } = props

	if (isAuth) {
		return <Redirect to='/' />
	}

	return (
		<div className='row d-flex justify-content-center'>
			<div className='col-8 col-md-7 col-lg-4 col-xl-4'>
				<TitleLarge>{t('login.title')}</TitleLarge>
				<TextMiddle className='mt-3'>
					{t('login.signUpText')}
					<NavLink to='/auth/signup'> {t('login.signUp')} </NavLink>
				</TextMiddle>
				<Formik
					initialValues={{
						email: '',
						password: '',
					}}
					validationSchema={Yup.object({
						password: Yup.string()
							.required('noProvided')
							.min(8, 'short')
							.matches(/[a-zA-Z]/, 'latin'),
						email: Yup.string()
							.email('invalidMail')
							.required('noProvidedEmail'),
					})}
					onSubmit={(value) => {
						const payload = {
							email: value.email,
							password: value.password,
						}
						loginWorker(payload)
					}}
				>
					{(props) => {
						const { values, handleChange } = props

						return (
							<Form className='d-flex flex-column'>
								<Input
									id='email'
									className='w-100'
									label={t('login.emailLabel')}
									name='email'
									type='text'
									value={values.email}
									onChange={handleChange}
								/>
								<Input
									id='password'
									className='w-100'
									label={t('login.passLabel')}
									type='password'
									name='password'
									value={values.password}
									onChange={handleChange}
								/>
								{/* <TextMiddle className='w-100 text-end mt-3'>
									<NavLink className='d-inline' to='/resetPassword'>
										{t('login.reset')}
									</NavLink>
								</TextMiddle> */}
								<div className='w-100 d-flex justify-content-center mt-3'>
									<CustomButton width='170px' className='mt-4' type='submit'>
										{t('header.login')}
									</CustomButton>
								</div>
							</Form>
						)
					}}
				</Formik>
			</div>
		</div>
	)
}

Login.propTypes = {
	isAuth: PropTypes.bool,
}

const mapStateToProps = createStructuredSelector({
	isAuth: select.isAuth,
})

const withConnect = connect(mapStateToProps, {
	loginWorker: actions.loginStart,
})

export default compose(memo, withConnect, withTranslation())(Login)
