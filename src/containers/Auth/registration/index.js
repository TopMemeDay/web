import React, { memo } from 'react'
import PropTypes from 'prop-types'
import { TitleLarge } from 'components/styled'
import { connect } from 'react-redux'
import { withTranslation } from 'react-i18next'
import { compose } from 'redux'
import { createStructuredSelector } from 'reselect'
import { select as auth_select } from 'containers/Auth/slices/selectors'
import { NavLink, Redirect } from 'react-router-dom'
import { Form, Formik } from 'formik'
import * as Yup from 'yup'
import Input from 'components/form/input'
import CustomButton from 'components/form/Button'
import { actions } from '../slices'

const validationSchema = Yup.object({
	nickname: Yup.string()
		.matches(/^(?!.*\.\.)(?!.*\.$)[^\W][\w.]{0,29}$/, 'notValid')
		.required('noProvNick'),
	password: Yup.string()
		.required('noProvided')
		.min(8, 'short')
		.matches(/[a-zA-Z]/, 'latin'),
	email: Yup.string().email('invalidMail').required('noProvidedEmail'),
})

function Registration(props) {
	const { isAuth, t, registerWorker } = props

	if (isAuth) {
		return <Redirect to='/' />
	}

	return (
		<div className='row d-flex justify-content-center'>
			<div className='col-8 col-md-7 col-lg-4 col-xl-4'>
				<TitleLarge>{t('signUp.title')}</TitleLarge>
				<div className='mt-3'>
					{t('login.signInText')}
					<NavLink to='/auth/signin'> {t('login.title')} </NavLink>
				</div>
				<Formik
					initialValues={{
						nickname: '',
						email: '',
						password: '',
					}}
					validationSchema={validationSchema}
					onSubmit={(value) => {
						registerWorker(value)
					}}
				>
					{(props) => {
						const { handleChange } = props

						return (
							<Form className='d-flex flex-column'>
								<Input
									id='nickname'
									className='w-100'
									label={t('signUp.nickNameLabel')}
									type='text'
									name='nickname'
									onChange={handleChange}
								/>
								<Input
									id='email'
									className='w-100'
									label={t('login.emailLabel')}
									name='email'
									type='text'
									onChange={handleChange}
								/>
								<Input
									id='password'
									className='w-100'
									label={t('login.passLabel')}
									type='password'
									name='password'
									onChange={handleChange}
								/>
								<div className='w-100 d-flex justify-content-center mt-3'>
									<CustomButton width='170px' className='mt-4' type='submit'>
										{t('login.signUp')}
									</CustomButton>
								</div>
							</Form>
						)
					}}
				</Formik>
			</div>
		</div>
	)
}

Registration.propTypes = {
	isAuth: PropTypes.bool,
}

const mapStateToProps = createStructuredSelector({
	isAuth: auth_select.isAuth,
})

const withConnect = connect(mapStateToProps, {
	registerWorker: actions.registerStart,
})

export default compose(memo, withConnect, withTranslation())(Registration)
