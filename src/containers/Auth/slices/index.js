import { createSlice } from '@reduxjs/toolkit'
import { UserModel } from '../../../models/user'
import { isAuth } from '../../../utils/auth'

const userAuth = createSlice({
	name: 'auth',
	initialState: {
		loading: false,
		isAuth: isAuth(),
		logOut: false,
		user: new UserModel(),
	},
	reducers: {
		loginStart: (state) => {
			state.loading = true
		},
		loginEnd: (state) => {
			state.loading = false
		},
		loginError: (state) => {
			state.loading = false
		},
		loginSuccess: (state, { payload }) => {
			state.user = new UserModel({ id: payload.id, ...state.user })
			state.isAuth = true
			state.loading = false
		},
		registerStart: (state) => {
			state.loading = true
		},
		registerEnd: (state) => {
			state.loading = false
		},
		registerSuccess: (state) => {
			state.loading = false
		},
		registerError: (state) => {
			state.loading = false
		},
		comeInSuccess: (state, { payload }) => {
			state.user = payload.user
		},
	},
})

export const { actions, reducer } = userAuth
