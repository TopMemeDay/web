import React from 'react'
import PropTypes from 'prop-types'
import { Redirect, Route, Switch } from 'react-router-dom'
import Login from './login'
import { shallowEqual, useSelector } from 'react-redux'
import Registration from './registration'

function Auth(props) {
	const { match } = props

	const { isAuth } = useSelector((state) => {
		return {
			isAuth: state.auth.isAuth,
		}
	}, shallowEqual)

	const path = {
		auth: `${match.path}`,
		login: `${match.path}/signin`,
		signup: `${match.path}/signup`,
	}

	if (isAuth) {
		return <Redirect to='/' />
	}

	return (
		<Switch>
			<Route
				exact
				path={path.auth}
				render={() => <Redirect to={path.login} />}
			/>
			<Route exact path={path.login} component={Login} />
			<Route exact path={path.signup} component={Registration} />
		</Switch>
	)
}

Auth.propTypes = {
	match: PropTypes.object,
}

export default Auth
