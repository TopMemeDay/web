export const AUTH_TOKEN = 'token'
export const MINE_TYPES = [
	'image/png',
	'image/pjpeg',
	'image/jpeg',
	'image/webp',
]
