import { colors } from 'components/variables'
import { css } from 'styled-components'
import Resizer from 'react-image-file-resizer'

export const textColor = (props) => {
	return css`
		color: ${props.color ? props.color : props.defaultColor};
		span {
			color: ${props.colorSpan && colors[props.colorSpan]};
		}
	`
}

const resizeFile = (file) =>
	new Promise((resolve) => {
		Resizer.imageFileResizer(
			file,
			680,
			400,
			'JPEG',
			100,
			0,
			(uri) => {
				resolve(uri)
			},
			'base64'
		)
	})
