import { UploadModel } from 'models/upload'
import Api from 'utils/api'

export async function uploadImage(payload) {
	const { file } = payload
	const formData = new FormData()

	formData.append('image', file, file.name)

	try {
		const { data } = await Api.post('/api/uploads', formData, {})
		return new UploadModel(data.upload)
	} catch (err) {
		console.error('Failed to upload image')
	}
}
