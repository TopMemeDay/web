import environment from 'environment'

export class PostModel {
	constructor(data = {}) {
		this._id = data._id || data._id
		this.likes = data.likes || []
		this.description = data.description || ''
		this.content =
			data.content.map((item) =>
				item.location ? `${environment.staticUrl + item.location}` : ''
			) || []
		this.author = data.author || ''
		this.likesCount = data.likesCount || 0
	}
}
