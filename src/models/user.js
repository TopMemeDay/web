import avatar from 'static/img/user.png'
import environment from 'environment'

export class UserModel {
	constructor(obj) {
		obj = obj || {}
		this.id = obj._id || obj.id
		this.email = obj.email || ''
		this.nickname = obj.nickname || ''
		this.avatarImage = obj?.avatarImage?.location
			? `${environment.staticUrl + obj.avatarImage.location}`
			: avatar
		this.avatarImageId = obj.avatarImageId || null
	}
}
