import { colors, maxW } from 'components/variables'
import styled from 'styled-components'
import { textColor } from 'utils/help-func'

export const PostsWrapper = styled.div``

export const PostWrapper = styled.div`
	margin-bottom: 14px;
`

export const Post = styled.div`
	text-align: center;
	height: 300px;
	background-color: ${colors.grey};
	/* background: linear-gradient(
		0deg,
		rgba(0, 0, 0, 0.3) 0%,
		rgba(255, 247, 247, 1) 50%,
		rgba(0, 0, 0, 0.3029586834733894) 100%
	); */
	position: relative;
	border-radius: 8px;
	overflow: hidden;
`

export const PostImg = styled.img`
	min-width: 100%;
	width: 100%;
	height: 100%;
	object-fit: cover;
`

export const PosiInfo = styled.div`
	display: flex;
	position: absolute;
	left: 0;
	right: 0;
	bottom: 0;
	justify-content: space-between;
	align-items: center;
	background: linear-gradient(rgba(0, 0, 0, 0) 0.65%, rgba(0, 0, 0, 1) 100%);
	padding: 0 15px 25px 15px;
	z-index: 4;
`

export const UserWrap = styled.div`
	align-items: center;
	display: flex;
	font-weight: 600;
	cursor: pointer;
	${(props) => textColor(props)}
	margin-bottom: 10px;
`
