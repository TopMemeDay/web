export const colors = {
	black: '#000000',
	white: '#ffffff',
	likeWhite: '#EBEBEB',
	grey: '#c7c7c7',
	blue: '#1877f2',
	red: '#FF3D3D',
	lightGrey: '#f1f1f1',
	orange: '#f57c00',
	darkGrey: '#303030',
}

export const fonts = {
	roboto: 'Roboto',
	comforta: 'Comfortaa',
}

export const size = {
	mobileS: '0',
	mobileM: '375',
	mobileL: '425',
	mobileXL: '480',
	tabletM: '640',
	tablet: '768',
	laptopS: '1024',
	laptopXS: '1200',
	laptopM: '1366',
	laptopL: '1440',
	desktop: '1920',
}

export const device = {
	mobileS: `(min-width: ${size.mobileS}px)`,
	tablet: `(min-width: ${size.tablet}px)`,
}
export const maxW = {
	mobileS: `(max-width: ${size.mobileS}px)`,
	laptopL: `(max-width: ${size.laptopL}px)`,
	mobileXL: `(max-width: ${size.mobileXL}px)`,
	tablet: `(max-width: ${size.tablet}px)`,
}
