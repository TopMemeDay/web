import React, { memo, useRef, useState } from 'react'
import notifier from 'simple-react-notifications'
import Cropper from 'cropperjs'
import { Icon } from 'components/icon'
import Modal from 'components/modal'
import Button from 'components/button'
import { ButtonWrap, SmallTitle } from 'components/styled'
import {
	Wrap,
	Inner,
	InnerModal,
	Camera,
	Text,
	CropWrap,
	CropImg,
	Ava,
	AvaImage,
	AvaCamera,
	AvaDelete,
} from './style'

const customStyles = {
	content: {
		borderRadius: '20px',
	},
}

const Upload = memo((props) => {
	const [visible, setVisible] = useState(false)
	const [file, setFile] = useState(null)
	const [cropInstance, setCropInstance] = useState(null)

	const inputEl = useRef(null)
	const imageRef = useRef(null)
	const canvasRef = useRef(null)

	const { text, image, mineTypes, onChange } = props

	let accept = mineTypes.reduce((acc, val) => `${acc},${val}`, '').slice(1)

	const handleClick = () => {
		inputEl.current.click()
	}

	const handleChange = (ev) => {
		if (ev.target.files && ev.target.files.length > 0) {
			const file = ev.target.files[0]

			if (mineTypes.indexOf(file.type) === -1) {
				notifier.error('File type unsupported')
			} else {
				const reader = new FileReader()

				reader.addEventListener('load', () => {
					setFile(file)
					handleLoadFile(reader.result)
				})

				reader.readAsDataURL(file)
			}
		}
		ev.target.value = ''
	}

	const handleLoadFile = (data) => {
		setVisible(true)
		setTimeout(() => {
			imageRef.current.src = data

			if (cropInstance) {
				cropInstance.destroy()
			}

			const cropper = new Cropper(imageRef.current, {
				aspectRatio: 1,
				viewMode: 1,
				crop(event) {
					handleLoadPreview(cropper.getCroppedCanvas())
				},
			})

			setCropInstance(cropper)
		}, 0)
	}

	const handleLoadPreview = (sourceCanvas) => {
		var canvas = canvasRef.current
		var context = canvas.getContext('2d')

		if (!context) {
			return
		}

		var width = window.innerWidth > 400 ? 200 : 100
		var height = window.innerWidth > 400 ? 200 : 100

		if (!sourceCanvas.width || !sourceCanvas.height) {
			return
		}

		canvas.width = width
		canvas.height = height
		context.imageSmoothingEnabled = true
		context.drawImage(sourceCanvas, 0, 0, width, height)
		context.globalCompositeOperation = 'destination-in'
		context.beginPath()
		// context.arc(width / 2, height / 2, Math.min(width, height) / 2, 0, 2 * Math.PI, true);
		context.fill()
	}

	const handleCancel = () => {
		if (cropInstance) {
			cropInstance.destroy()
		}

		setVisible(false)
	}

	const handleUpload = () => {
		canvasRef.current.toBlob(function (blob) {
			setVisible(false)
			setFile(null)
			setCropInstance(null)
			onChange({
				file: blob,
				name: file.name,
				type: file.type,
			})
		}, file.type)
	}

	const handleDelete = (ev) => {
		ev.preventDefault()
		ev.stopPropagation()

		props.onDelete()
	}

	return (
		<Wrap>
			<Inner onClick={handleClick}>
				{image && (
					<Ava>
						<AvaImage alt='' src={image} />
						<AvaCamera>
							<Icon component='Camera' />
						</AvaCamera>
						<AvaDelete onClick={handleDelete}>
							<Icon component='Trash' />
						</AvaDelete>
					</Ava>
				)}
				{!image && (
					<Camera>
						<Icon component='Camera' />
						<Text>{text}</Text>
					</Camera>
				)}
			</Inner>
			<input
				className='d-none'
				ref={inputEl}
				type='file'
				accept={accept}
				onChange={handleChange}
			/>
			<Modal visible={visible} style={customStyles}>
				<InnerModal>
					<CropWrap>
						<div>
							<CropImg ref={imageRef} alt='' />
						</div>
					</CropWrap>
					<div>
						<div className='text-center'>
							<SmallTitle>Preview</SmallTitle>
						</div>
						<div className='mt-4 mb-4 d-flex justify-content-center'>
							<canvas style={{ borderRadius: '50%' }} ref={canvasRef} />
						</div>
					</div>
					<ButtonWrap>
						<div className='d-f justify-beetwen'>
							<Button theme='text' onClick={handleCancel}>
								Cancel
							</Button>
							<Button onClick={handleUpload}>Upload</Button>
						</div>
					</ButtonWrap>
				</InnerModal>
			</Modal>
		</Wrap>
	)
})

Upload.defaultProps = {
	text: 'Click to upload photo',
}

export default Upload
