import { UploadPlusIcon } from 'components/Icons/data'
import React, { useRef } from 'react'
import notifier from 'simple-react-notifications'
import { UploadButton, UploadPlus } from './style'
import Notification from 'components/notification'

function MultipleUpload({ images, setImages, mineTypes }) {
	let accept = mineTypes.reduce((acc, val) => `${acc},${val}`, '').slice(1)
	const fileRef = useRef(null)

	const handleUpload = () => {
		fileRef.current.click()
	}

	const handleChange = (e) => {
		let count = images.length
		for (const value of e.target.files) {
			if (count >= 5) {
				notifier({
					single: true,
					position: 'top-right',
					delay: 500,
					render: ({ id, onClose }) => (
						<Notification
							key={id}
							onClosePanel={onClose}
							content={'So Many files'}
						/>
					),
				})
				break
			} else if (mineTypes.indexOf(value.type) === -1) {
				notifier({
					single: true,
					position: 'top-right',
					delay: 500,
					render: ({ id, onClose }) => (
						<Notification
							key={id}
							onClosePanel={onClose}
							content={'Bad type of file'}
						/>
					),
				})
				break
			}
			setImages({ file: value, image: URL.createObjectURL(value) })
			count++
		}
	}

	return (
		<>
			<input
				type='file'
				ref={fileRef}
				onChange={handleChange}
				multiple
				className='d-none'
				accept={accept}
			/>
			{images.length !== 5 && (
				<UploadButton onClick={handleUpload}>
					<UploadPlus>
						<UploadPlusIcon />
					</UploadPlus>
				</UploadButton>
			)}
		</>
	)
}

export default MultipleUpload
