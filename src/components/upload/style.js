import { colors, maxW } from 'components/variables'
import styled from 'styled-components'

export const UploadButton = styled.div`
	position: relative;
	width: 100px;
	height: 120px;
	border-radius: 4px;
	background-color: ${colors.lightGrey};
	cursor: pointer;
	transition: 0.5s;
	&:hover {
		background-color: ${colors.grey};
	}
	@media ${maxW.mobileXL} {
		width: 60px;
		height: 80px;
	}
`

export const UploadPlus = styled.div`
	height: 32px;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
`
