import { colors, maxW } from 'components/variables'
import styled from 'styled-components'

export const TextareaWrapp = styled.div`
	margin: 10px;
	margin-bottom: 25px;
`

export const TextareaInput = styled.textarea`
	resize: vertical;
	width: 100%;
	min-height: 150px;
	padding: 10px;
	border: 1px solid ${colors.grey};
	border-radius: 4px;
	font-family: 'Poppins', sans-serif;
	font-size: 15px;
	@media ${maxW.mobileXL} {
		min-height: 70px;
		font-size: 14px;
	}
`

export const TextareaLabel = styled.label`
	font-size: 16px;
	color: ${colors.black};
`
