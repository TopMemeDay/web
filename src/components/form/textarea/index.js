import { useField } from 'formik'
import { withTranslation } from 'react-i18next'
import { ErrorText, Label } from '../input/style'
import { TextareaInput, TextareaLabel, TextareaWrapp } from './style'

const Textarea = (props) => {
	const [field, meta] = useField({ ...props })

	const { label, className, id, t } = props

	const { value } = field

	const error = meta.touched && meta.error

	return (
		<TextareaWrapp>
			{/* {label && <TextareaLabel htmlFor={id}>{label}</TextareaLabel>} */}
			<TextareaInput
				type={field.type}
				id={id}
				className={className}
				{...field}
				{...props}
				{...meta}
				value={value}
			/>
			{error && <ErrorText>{t(`error.${meta.error}`)}</ErrorText>}
		</TextareaWrapp>
	)
}

export default withTranslation()(Textarea)
