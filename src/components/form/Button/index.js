import React from 'react'
import styled from 'styled-components'
import { colors, maxW } from 'components/variables'

const Button = styled.button`
	padding: 15px 10px;
	border: 1px solid ${colors.black};
	background-color: ${colors.white};
	font-size: 16px;
	width: ${(props) => props.width || 'auto'};
	border-radius: 30px;
	color: ${colors.black};
	transition: all 0.3s;
	text-align: center;
	&:hover {
		border-color: ${colors.blue};
		color: ${colors.blue};
		cursor: pointer;
	}
	@media ${maxW.tablet} {
		padding: 5px 0;
		min-width: 150px;
	}
`

const CustomButton = ({ children, ...props }) => (
	<Button {...props}>{children}</Button>
)

CustomButton.propTypes = {}

export default CustomButton
