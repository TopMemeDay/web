import { useField } from 'formik'
import React from 'react'
import { withTranslation } from 'react-i18next'
import styled from 'styled-components'

const RadioStyle = styled.input`
	width: 20px;
	height: 20px;
	margin-right: 10px;
`

const Lablel = styled.span`
	font-size: 20px;
`

const Wrapper = styled.div`
	display: flex;
	align-items: center;
	justify-content: space-between;
`

function Radio({ label, t, ...props }) {
	const [field] = useField(props)

	return (
		<Wrapper>
			<RadioStyle type={props.type} {...field} />
			{label && <Lablel>{label}</Lablel>}
		</Wrapper>
	)
}

export default withTranslation()(Radio)
