import styled from 'styled-components'
import { colors } from 'components/variables'

const { blue, red } = colors

export const CheckWrap = styled.div`
	.checkbox {
		position: relative;
		display: block;
		&::before {
			position: absolute;
			display: flex;
			content: '';
			height: 16px;
			width: 16px;
			border-radius: 5px;
			background: transparent;
			border: 1px solid ${blue};
			left: 0;
			top: calc(50% - 16px / 2);
		}
		&::after {
			content: none;
			cursor: pointer;
			position: absolute;
			height: 5px;
			width: 9px;
			border-left: 2px solid #fff;
			border-bottom: 2px solid #fff;
			border-radius: 3px;
			transform: rotate(-45deg);
			left: 4px;
			top: calc(50% - 6px / 2);
		}
		&.active::after {
			content: '';
		}
		&.active::before {
			background: ${blue};
		}
	}
	input[type='checkbox'] {
		opacity: 0;
		position: absolute;
	}
	.error {
		margin-top: 10px;
		color: ${red};
	}
`
