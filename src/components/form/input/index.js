import { InvisiblePassword, VisiblePassword } from 'components/Icons/data'
import { useField } from 'formik'
import { useState } from 'react'
import { withTranslation } from 'react-i18next'
import { ErrorText, Input, InputWrapper, Label, SeePassword } from './style'

const BasicInput = (props) => {
	const [field, meta] = useField({ ...props })

	const { label, className, id, t } = props

	const password = props.type === 'password'

	const { value } = field

	const [focus, setFocus] = useState(false)
	const [seePassword, setSeePassword] = useState(false)

	const error = meta.touched && meta.error

	const handleAutoFill = (e) => {
		setFocus(e.animationName === 'onAutoFillStart')
	}

	return (
		<InputWrapper>
			<Input
				type={password ? (seePassword ? 'text' : 'password') : field.type}
				id={id}
				className={className}
				{...field}
				{...meta}
				onFocus={() => setFocus(true)}
				onBlur={() => setFocus(!!value)}
				focus={focus}
				value={value}
				onAnimationStart={handleAutoFill}
			/>
			{label && (
				<Label value={value} htmlFor={id} focus={focus}>
					{label}
				</Label>
			)}
			{password && (
				<SeePassword onClick={() => setSeePassword(!seePassword)}>
					{seePassword ? <InvisiblePassword /> : <VisiblePassword />}
				</SeePassword>
			)}
			{error && <ErrorText>{t(`error.${meta.error}`)}</ErrorText>}
		</InputWrapper>
	)
}

export default withTranslation()(BasicInput)
