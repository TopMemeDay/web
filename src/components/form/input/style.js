import { colors, device } from 'components/variables'
import styled, { css } from 'styled-components'

export const InputWrapper = styled.div`
	position: relative;
	display: block;
	margin-top: 50px;
`

export const Input = styled.input`
	position: relative;
	border: none;
	border-bottom: 1px solid ${colors.black};
	&:focus {
		border-bottom: 1px solid ${colors.blue};
	}
	&:-webkit-autofill {
		-webkit-box-shadow: 0 0 0px 1000px white inset;
		animation-name: onAutoFillStart;
	}
	&:-webkit-autofill:focus {
		-webkit-box-shadow: 0 0 0 50px white inset;
		-webkit-text-fill-color: ${colors.black};
		animation-name: onAutoFillStart;
	}
	&:-webkit-autofill,
	&:-webkit-autofill:hover,
	&:-webkit-autofill:focus,
	&:-webkit-autofill:active {
		animation-name: onAutoFillStart;
		transition: background-color 5000s ease-in-out 0s;
	}
	@keyframes onAutoFillStart {
	}
`

export const Label = styled.label`
	position: absolute;
	bottom: 5px;
	right: auto;
	left: 0;
	z-index: 9;
	transition-timing-function: cubic-bezier(0.25, 0.8, 0.25, 1);
	transition: all 0.4s;
	font-size: 14px;
	${(props) =>
		(props.focus || props.value !== '') &&
		css`
			font-size: 12px !important;
			transform: translate3d(0, -28px, 0) scale(1);
			transform-origin: left top;
		`};
	@media ${device.tablet} {
		font-size: 16px;
	}
`

export const ErrorText = styled.div`
	color: ${colors.red};
	font-size: 12px;
	position: absolute;
`

export const SeePassword = styled.div`
	position: absolute;
	bottom: 0;
	right: 0;
`
