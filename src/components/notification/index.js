// import { colors } from 'components/variables'
// import { memo } from 'react'
// import { React } from 'react'
// import styled from 'styled-components'

// const NotifWrapp = styled.div`
// 	background: ${colors.strongPink};
// 	color: white;
// 	padding: 8px 16px;
// 	position: absolute;
// 	top: 100px;
// 	right: 20px;
// 	border-radius: 20px;
// 	padding: 11px 15px;
// 	font-size: 11px;
// `

// const Notifier = ({ content, onClosePanel, ...props }) => {
// 	return (
// 		<NotifWrapp onClick={onClosePanel} {...props}>
// 			{content}
// 		</NotifWrapp>
// 	)
// }

// export default memo(Notifier)

import { CloseIcon } from 'components/Icons/data'
import React, { memo } from 'react'
import styled from 'styled-components'

const Toast = styled.div`
	display: flex;
	justify-content: start;
	text-align: left;
	background: ${(props) => (props.type === 'error' ? '#FF424C' : '#0F5AE3')};
	color: white;
	padding: 20px 40px 20px 30px;
	position: fixed;
	top: 10px;
	right: 0;
	z-index: 9999999;
	box-shadow: ${(props) =>
		props.type === 'error' ? '0px 0px 5px #FF424C' : '0px 0px 5px #65D8FD'};
	margin-bottom: 10px;
	border-radius: 10px;
`

const CustomNotifier = ({ content, onClosePanel, type }) => (
	<Toast type={type} className='route-info'>
		<span className='subtitle'>{content}</span>
		<div
			className='button'
			style={{
				position: 'absolute',
				top: '10px',
				right: '15px',
				cursor: 'pointer',
			}}
			onClick={onClosePanel}
		>
			<CloseIcon width='20px' height='20px' />
		</div>
	</Toast>
)

export default memo(CustomNotifier)
