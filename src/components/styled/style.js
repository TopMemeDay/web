import { colors, device, maxW } from 'components/variables'
import styled, { css } from 'styled-components'

const textColor = (props) => {
	return css`
		color: ${props.color ? colors[props.color] : colors[props.defaultColor]};
		span {
			color: ${props.colorSpan && colors[props.colorSpan]};
		}
	`
}

export const TitleLarge = styled.h1`
	color: ${colors.black};
	text-align: center;
`

export const TextMiddle = styled.div`
	${(p) => textColor({ defaultColor: colors.black })}
	font-size: 14px;
	@media ${device.tablet} {
		font-size: 16px;
	}
`
