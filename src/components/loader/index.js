import React from 'react'
import { useLoading, ThreeDots } from '@agney/react-loading'
import styled from 'styled-components'

const LoaderWrapp = styled.div`
	width: 100%;
	text-align: center;
`

function Loader(props) {
	const { containerProps, indicatorEl } = useLoading({
		loading: true,
		indicator: <ThreeDots width='50' />,
	})
	return (
		<LoaderWrapp {...props} {...containerProps}>
			{indicatorEl}
		</LoaderWrapp>
	)
}

export default Loader
