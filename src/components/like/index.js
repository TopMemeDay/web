import Notification from 'components/notification'
import { colors, maxW } from 'components/variables'
import { select } from 'containers/Auth/slices/selectors'
import { actions, postSelect } from 'containers/Home/slices'
import React, { memo, useState } from 'react'
import { withTranslation } from 'react-i18next'
import { connect, useStore } from 'react-redux'
import { compose } from 'redux'
import { createStructuredSelector } from 'reselect'
import styled from 'styled-components'
import notifier from 'simple-react-notifications'
import { textColor } from 'utils/help-func'

const LikeWrapp = styled.div`
	${(p) => p.isAuth && 'cursor: pointer;'}
	display: flex;
	align-items: center;
	${(props) => textColor(props)}
	font-size: 16px;
	@media ${maxW.tablet} {
		font-size: 14px;
	}
`

const Heart = ({ size, active, color, loading, ...props }) => {
	let width = size === 'small' ? 14 : 22
	let height = size === 'small' ? 12 : 18
	return (
		<svg
			{...props}
			width={width}
			height={height}
			viewBox='0 0 22 18'
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			style={{ marginRight: '10px' }}
		>
			<path
				fillRule='evenodd'
				clipRule='evenodd'
				d='M11.468 1.93675L10.7814 2.61896L10.0992 1.93675C7.8537 -0.30704 4.21487 -0.30704 1.96942 1.93675C-0.188735 4.09792 -0.282795 7.56856 1.75513 9.84345C4.27058 12.6322 7.03573 15.1851 10.0161 17.4703C10.4696 17.8151 11.0975 17.8151 11.551 17.4703C14.5323 15.1862 17.2976 12.6332 19.812 9.84345C21.8499 7.56856 21.7558 4.09792 19.5977 1.93675C17.3522 -0.30704 13.7134 -0.30704 11.468 1.93675Z'
				fill={active ? colors.red : loading ? colors.grey : colors.white}
			/>
		</svg>
	)
}

function Like({
	isLiked,
	likeCount = 0,
	postId,
	isAuth,
	onLike,
	offLike,
	loading,
	t,
	color,
	...props
}) {
	const [liked, setLiked] = useState(isLiked)
	const [count, setCount] = useState(likeCount)

	const handleLike = (e) => {
		e.stopPropagation()
		if (!isAuth) {
			notifier({
				single: true,
				position: 'top-right',
				delay: 500,
				render: ({ id, onClose }) => (
					<Notification key={id} onClosePanel={onClose} content={t()} />
				),
			})
		}
		if (!loading) {
			setLiked(!liked)
			liked ? offLike({ _id: postId }) : onLike({ _id: postId })
			liked ? setCount(count - 1) : setCount(count + 1)
		}
	}

	return (
		<LikeWrapp
			isAuth={isAuth}
			color={color}
			defaultColor={colors.white}
			onClick={handleLike}
		>
			<Heart active={liked} />
			{count > 0 && count}
		</LikeWrapp>
	)
}

const mapStateToProps = createStructuredSelector({
	isAuth: select.isAuth,
	loading: postSelect.loading,
})

const withConnect = connect(mapStateToProps, {
	onLike: actions.onLike,
	offLike: actions.offLike,
})

export default compose(memo, withConnect, withTranslation())(Like)
