import { AvatarMini } from 'components/Avatar'
import { CloseIcon } from 'components/Icons/data'
import Like from 'components/like'
import { PosiInfo, UserWrap } from 'components/posts/style'
import { TextMiddle } from 'components/styled'
import { colors } from 'components/variables'
import React from 'react'
import { Link } from 'react-router-dom'
import Slider from 'react-slick'
import {
	CloseModal,
	ModalDesc,
	ModalImg,
	ModalImgWrapp,
	ModalWrapp,
} from './style'

import avatar from 'static/img/user.png'

const settings = {
	dots: true,
	infinite: true,
	speed: 500,
	slidesToShow: 1,
	slidesToScroll: 1,
	adaptiveHeight: true,
}

export function BasicModal(props) {
	const { onClose, post } = props
	const { author, likedCount, likes, description, content, _id } = post
	const liked = likes.find((element) => element === author._id)

	return (
		<ModalWrapp>
			<ModalImgWrapp>
				<Slider {...settings}>
					{content.map((item, _index) => (
						<ModalImg key={_index} src={item} alt='SMT GO WRONG' />
					))}
				</Slider>
				<PosiInfo>
					<Like isLiked={liked} likeCount={likedCount} postId={_id} />
				</PosiInfo>
			</ModalImgWrapp>
			<ModalDesc>
				<UserWrap color={colors.black}>
					<Link to={`/profile/${author}`}>
						<AvatarMini className='mr-2' image={author.image || avatar} />
						<TextMiddle color={colors.black}>{author.name}</TextMiddle>
					</Link>
				</UserWrap>
				{description}
			</ModalDesc>
			<CloseModal onClick={onClose}>
				<CloseIcon />
			</CloseModal>
		</ModalWrapp>
	)
}
