import { colors, maxW } from 'components/variables'
import styled from 'styled-components'

export const ModalWrapp = styled.div`
	width: 810px;
	position: relative;
	overflow: hidden;
	overflow-y: auto;
	@media ${maxW.laptopL} {
		width: 630px;
	}
	@media ${maxW.tablet} {
		width: 410px;
	}
	@media ${maxW.mobileXL} {
		width: 100%;
	}
	.bg-grey {
		background-color: ${colors.lightGrey};
	}
	&::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
		background-color: #f5f5f5;
	}

	&::-webkit-scrollbar {
		width: 5px;
		background-color: #f5f5f5;
	}

	&::-webkit-scrollbar-thumb {
		background-color: #000000;
		border-radius: 10px;
		margin-top: 10px;
	}
`

export const ModalImgWrapp = styled.div`
	position: relative;
`

export const ModalImg = styled.img`
	max-height: 500px;
	object-fit: contain;
`

export const ModalDesc = styled.div`
	height: 30%;
	width: 100%;
	padding: 15px;
	box-sizing: border-box;
	background-color: ${colors.white};
	font-size: 16px;
`

export const CloseModal = styled.div`
	position: fixed;
	top: 16px;
	right: 25px;
	cursor: pointer;
	content: '';
	@media ${maxW.mobileXL} {
		top: 15px;
		right: 15px;
	}
`

// Create Post Modal

export const ImagesWrapper = styled.div`
	min-height: 264px;
	display: flex;
	flex-wrap: wrap;
	align-items: center;
	justify-content: center;
	border-bottom: 1px solid ${colors.black};
	padding: 70px 0;
	@media ${maxW.mobileXL} {
		padding-top: 32px;
		padding-bottom: 5px;
		min-height: 150px;
	}
`

export const SingleImgWrapp = styled.div`
	width: 115px;
	height: 140px;
	margin: 5px;
	position: relative;
	border-radius: 4px;
	overflow: hidden;
	&::after {
		position: absolute;
		content: '';
		top: 0;
		bottom: 0;
		right: 0;
		left: 0;
		z-index: 11;
		background: rgba(0, 0, 0, 0.3);
	}
	@media ${maxW.mobileXL} {
		width: 60px;
		height: 80px;
	}
`

export const SingleImg = styled.img`
	width: 100%;
	float: left;
	object-fit: cover;
	height: 100%;
	z-index: 10;
`

export const SingleDel = styled.div`
	position: absolute;
	top: 7px;
	right: 7px;
	cursor: pointer;
	content: '';
	width: 20px;
	height: 20px;
	background-color: ${colors.black};
	border-radius: 50%;
	z-index: 12;
	&:before,
	&:after {
		position: absolute;
		left: 46%;
		top: 17%;
		content: ' ';
		height: 15px;
		width: 2px;
		background-color: ${colors.white};
		transform: translate(-50%, -50%);
	}
	&:before {
		transform: rotate(45deg);
	}
	&:after {
		transform: rotate(-45deg);
	}
	@media ${maxW.mobileXL} {
		top: 3px;
		right: 3px;
		&:before,
		&:after {
			height: 10px;
			top: 21%;
		}
	}
`

export const ButtonWrapp = styled.div`
	max-width: 150px;
	margin: 0 auto 20px auto;
`
