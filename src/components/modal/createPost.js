import { Form, Formik } from 'formik'
import * as Yup from 'yup'
import React, { memo, useState } from 'react'
import {
	ButtonWrapp,
	CloseModal,
	ImagesWrapper,
	ModalWrapp,
	SingleDel,
	SingleImg,
	SingleImgWrapp,
} from './style'
import Textarea from 'components/form/textarea'
import { MINE_TYPES } from 'utils/constans'
import MultipleUpload from 'components/upload/multipleUpload'
import { withTranslation } from 'react-i18next'
import Button from '@material-ui/core/Button'
import { compose } from 'redux'
import { postSelect } from 'containers/Home/slices'
import { createStructuredSelector } from 'reselect'
import { actions as postActions } from 'containers/Home/slices'
import { connect } from 'react-redux'
import CustomButton from 'components/form/Button'
import { CloseIcon } from 'components/Icons/data'

const settings = {
	dots: true,
	infinite: true,
	speed: 500,
	slidesToShow: 1,
	slidesToScroll: 1,
	adaptiveHeight: true,
}

function CreatePost(props) {
	const { onClose, mineTypes, t, loading, createAction } = props
	const [images, setImages] = useState([])

	const handleSetImages = (newImage) => {
		setImages((prevState) => [...prevState, newImage])
	}

	const handleDeleteImage = (item) => () => {
		setImages(images.filter((element) => element !== item))
	}

	return (
		<ModalWrapp>
			<ImagesWrapper>
				{images.map((item, _index) => (
					<SingleImgWrapp key={_index}>
						<SingleDel onClick={handleDeleteImage(item)} />
						<SingleImg src={item.image} />
					</SingleImgWrapp>
				))}
				<MultipleUpload
					mineTypes={mineTypes}
					images={images}
					setImages={handleSetImages}
				/>
			</ImagesWrapper>
			<Formik
				initialValues={{
					description: '',
				}}
				validationSchema={Yup.object({
					description: Yup.string().required('requiredField'),
				})}
				onSubmit={(value, { resetForm }) => {
					const payload = {
						images,
						description: value.description,
					}
					resetForm()
					setImages([])
					createAction({ payload, next: onClose() })
					// loginWorker(payload)
				}}
			>
				{(props) => {
					const { values, handleChange } = props

					return (
						<Form className='d-flex flex-column bg-grey'>
							<Textarea
								id='description'
								className='w-100'
								placeholder={t('posts.textareaLabel')}
								name='description'
								type='text'
								value={values.description}
								onChange={handleChange}
							/>
							<ButtonWrapp>
								<CustomButton disabled={loading} type='submit' width='170px'>
									Create
								</CustomButton>
							</ButtonWrapp>
						</Form>
					)
				}}
			</Formik>
			<CloseModal onClick={onClose}>
				<CloseIcon />
			</CloseModal>
		</ModalWrapp>
	)
}

CreatePost.defaultProps = {
	mineTypes: MINE_TYPES,
}

const mapStateToProps = createStructuredSelector({
	loading: postSelect.loading,
})

const withConnect = connect(mapStateToProps, {
	createAction: postActions.createStart,
})

export default compose(withConnect, memo, withTranslation())(CreatePost)
