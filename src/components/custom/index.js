import { size } from 'components/variables'
import styled from 'styled-components'

export const Container = styled.div`
	max-width: ${size.laptopL}px;
	margin: 0 auto;
`
