import styled from 'styled-components'

export const LargeAvatar = styled.img`
	object-fit: cover;
	width: 150px;
	height: 150px;
	border-radius: 50%;
`
