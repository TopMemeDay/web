import React from 'react'
import PropTypes from 'prop-types'
import { AvatarMiniStyle } from './style'

const AvatarMini = ({ image, ...props }) => (
	<AvatarMiniStyle {...props} src={image} alt='NoImg' />
)

AvatarMini.propTypes = {
	user: PropTypes.object,
}

export { AvatarMini }
