import styled from 'styled-components'

export const AvatarMiniStyle = styled.img`
	width: 40px;
	height: 40px;
	border-radius: 100%;
`
