import { Galary, PlusIconBar } from 'components/Icons/data'
import { select as userSelect } from 'containers/Profile/slices'
import { select as authSelect } from 'containers/Auth/slices/selectors'
import React, { useState } from 'react'
import { useEffect } from 'react'
import { withTranslation } from 'react-i18next'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { createStructuredSelector } from 'reselect'
import history from 'utils/history'
import {
	FilterButton,
	FilterContainer,
	FilterItem,
	FilterSlider,
	FilterSliderItem,
	FilterTabs,
	FilterWrapper,
	Tabs,
	TabsAvatar,
	TabsAvatarImg,
	TabsButton,
	TabsLi,
	TabsNav,
	TabsSlide,
	TabsSlideCircle,
	TabsUl,
} from './style'

const dataFilter = [
	{ label: 'week', value: '0' },
	{ label: 'month', value: '100%' },
	{ label: 'all', value: '200%' },
]

const dataTab = [
	{
		icon: <Galary />,
		colors: {
			main: '#00c853',
			_50: '#e8f5e9',
			_100: '#c8e6c9',
		},
		value: '52%',
		isSubMenu: true,
		link: false,
		allowUntorized: true,
	},
	{
		icon: <PlusIconBar />,
		colors: {
			main: '#ff6d00',
			_50: '#fff3e0',
			_100: '#ffe0b2',
		},
		value: '152%',
		isSubMenu: false,
		link: false,
		allowUntorized: false,
	},
	{
		image:
			'https://images.pexels.com/photos/3756985/pexels-photo-3756985.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=50' ||
			true,
		colors: {
			main: '#2962ff',
			_50: '#e3f2fd',
			_100: '#bbdefb',
		},
		value: '252%',
		isSubMenu: false,
		link: '/profile',
		allowUntorized: false,
	},
]

function Sidebar({ openCreateModal, t, isAuth, isOpen, user, ...props }) {
	const [activeTab, setTab] = useState(dataTab[0])
	const [activeFilter, setFilter] = useState(dataFilter[0])
	const [openSub, setSub] = useState(true)

	const handleSetTab = (item) => (e) => {
		if (activeTab === item) {
			return
		}
		setTab(item)
		item.isSubMenu ? setSub(true) : setSub(false)
		item === dataTab[1] && openCreateModal()
		if (item.link) {
			history.push(item.link)
		}
	}

	const handleSetFilter = (item) => () => {
		if (activeFilter === item) {
			return
		}
		setFilter(item)
		history.push({
			pathname: `/home/${item.label}`,
		})
	}

	const RenderItems = (data) => {
		return data.map((item, _index) => {
			const isActive = item === activeTab
			return (
				<TabsLi key={_index}>
					<TabsButton
						color={isActive ? item.colors.main : ''}
						onClick={handleSetTab(item)}
					>
						{item.image ? (
							<TabsAvatar>
								<TabsAvatarImg src={user.avatarImage} />
							</TabsAvatar>
						) : (
							item.icon
						)}
					</TabsButton>
				</TabsLi>
			)
		})
	}

	const filterItems = (shouldFilter = false, filter = '', items = []) => {
		let itemsToReturn
		if (shouldFilter) {
			itemsToReturn = items.filter((value) => value[filter])
		} else {
			return items
		}

		return itemsToReturn
	}

	useEffect(() => {
		handleSetTab(isOpen ? dataTab[1] : dataTab[0])()
	}, [isOpen])

	return (
		<TabsNav>
			<FilterContainer open={openSub}>
				<FilterWrapper open={openSub}>
					<FilterTabs>
						{dataFilter.map((item, _index) => {
							return (
								<FilterItem key={_index}>
									<FilterButton
										onClick={handleSetFilter(item)}
										open={activeFilter === item}
									>
										{t(`footer.${item.label}`)}
									</FilterButton>
								</FilterItem>
							)
						})}
					</FilterTabs>
					<FilterSlider item={activeFilter}>
						<FilterSliderItem item={activeFilter}>&nbsp;</FilterSliderItem>
					</FilterSlider>
				</FilterWrapper>
			</FilterContainer>
			<Tabs>
				<TabsUl>
					{isAuth
						? RenderItems(filterItems(false, null, dataTab))
						: RenderItems(filterItems(true, 'allowUntorized', dataTab))}
					{/* {isAuth ? RenderItems(filteredItemsAuth) : RenderItems(filteredItems)} */}
				</TabsUl>
				<TabsSlide item={activeTab} isAuth={isAuth}>
					<TabsSlideCircle item={activeTab}>&nbsp;</TabsSlideCircle>
				</TabsSlide>
			</Tabs>
		</TabsNav>
	)
}

const mapStateToProps = createStructuredSelector({
	isAuth: authSelect.isAuth,
	user: userSelect.user,
})

const withConnect = connect(mapStateToProps, {})

export default compose(withConnect, withTranslation())(Sidebar)
