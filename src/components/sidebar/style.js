import { colors } from 'components/variables'
import styled from 'styled-components'

export const TabsNav = styled.nav`
	background-color: ${colors.white};
	border-radius: 25px 25px 0 0;
	user-select: none;
	padding-top: 10px;
	min-width: 315px;
`

export const Tabs = styled.div`
	position: relative;
`

export const TabsUl = styled.ul`
	list-style-type: none;
	display: flex;
	justify-content: center;
`

export const TabsLi = styled.li`
	display: inline-flex;
	position: relative;
	padding: 15px;
	z-index: 1;
`

export const TabsButton = styled.button`
	border: none;
	height: 48px;
	width: 48px;
	border-radius: 50%;
	display: inline-flex;
	align-items: center;
	justify-content: center;
	color: ${(props) => props.color || colors.grey};
	transition: color 0.2s ease-in-out;
	background-color: transparent;
	position: relative;
	svg {
		pointer-events: none;
		height: 28px;
		width: 28px;
		transform: translate(0, 0);
		position: absolute;
	}
	@-moz-document url-prefix() {
		svg {
			-webkit-transform: translate(-50%, -50%);
		}
	}
`

export const TabsAvatar = styled.span`
	height: 40px;
	width: 40px;
	border-radius: 50%;
	pointer-events: none;
`

export const TabsAvatarImg = styled.img`
	height: 40px;
	width: 40px;
	border-radius: 50%;
	pointer-events: none;
	object-fit: cover;
`

export const TabsSlide = styled.div`
	pointer-events: none;
	position: absolute;
	top: 0;
	left: 0;
	padding: 15px;
	z-index: 0;
	transition: transform 0.4s ease-in-out;
	transform: ${(props) =>
		`translateX(${props.isAuth ? props.item.value : '152%'})`};
`

export const TabsSlideCircle = styled.div`
	height: 48px;
	width: 48px;
	border-radius: 50%;
	transition: background-color 0.4s ease-in-out;
	background-color: ${(props) => props.item.colors._50};
	${(props) => {}}
`

// Filter ---------------

export const FilterContainer = styled.div`
	overflow: hidden;
	padding: 0 10px;
	transition: max-height 0.4s ease-in-out;
	max-height: ${(props) => (props.open ? '3.8rem' : '0')};
`

export const FilterWrapper = styled.div`
	position: relative;
	transition: opacity 0.2s ease-in-out;
	opacity: ${(props) => (props.open ? '1' : '0')};
`

export const FilterTabs = styled.ul`
	border-radius: 10px;
	padding: 3px;
	overflow: hidden;
	background-color: ${colors.darkGrey};
	display: flex;
`

export const FilterItem = styled.li`
	position: relative;
	z-index: 1;
	display: flex;
	flex: 1 0 33.33%;
`

export const FilterButton = styled.button`
	border: none;
	display: flex;
	align-items: center;
	justify-content: center;
	border-radius: 8px;
	flex-grow: 1;
	height: 30px;
	padding: 0 10px;
	transition: color 0.4s ease-in-out;
	color: ${(props) => (!props.open ? colors.white : colors.black)};
	background-color: transparent;
	font-family: 'Open Sans', sans-serif;
	font-weight: 400;
	font-size: 14px;
`

export const FilterSlider = styled.div`
	pointer-events: none;
	position: absolute;
	padding: 3px;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 0;
`

export const FilterSliderItem = styled.div`
	height: 30px;
	width: 33.33%;
	border-radius: 8px;
	background-color: ${colors.white};
	box-shadow: 0 1px 10px -4px rgba(0, 0, 0, 0.12);
	transition: transform 0.4s ease-in-out;
	transform: ${(props) => `translateX(${props.item.value})`};
`
