import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { isAuth } from 'utils/auth'

const PrivateRouter = ({ component: Component, children, ...rest }) => (
	<Route
		{...rest}
		render={(props) => {
			return isAuth() ? (
				children ? (
					children
				) : (
					<Component {...props} />
				)
			) : (
				<Redirect
					to={{
						pathname: `/auth`,
						state: { from: props.location },
					}}
				/>
			)
		}}
	/>
)

export default PrivateRouter
