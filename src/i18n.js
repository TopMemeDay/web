import i18n from 'i18next'
import LanguageDetector from 'i18next-browser-languagedetector'
import { initReactI18next } from 'react-i18next'

import en from 'static/locales/en/en.json'
import uk from 'static/locales/uk/uk.json'
import ru from 'static/locales/ru/ru.json'

const resources = {
	en: {
		translation: en,
	},
	uk: {
		translation: uk,
	},
	ru: {
		translation: ru,
	},
}

i18n
	.use(LanguageDetector)
	.use(initReactI18next)
	.init({
		resources,
		detection: {
			order: ['queryString', 'cookie'],
			cache: ['cookie'],
		},
		fallbackLng: 'en',
		debug: false,
		keySeparator: '.',
		interpolation: {
			escapeValue: false,
		},
	})

export default i18n
