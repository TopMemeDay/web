import { all, fork } from 'redux-saga/effects'

import authSaga from 'containers/Auth/saga'
import postSaga from 'containers/Home/saga'
import profileSaga from 'containers/Profile/saga'

export default function* () {
	yield all([fork(postSaga), fork(authSaga), fork(profileSaga)])
}
