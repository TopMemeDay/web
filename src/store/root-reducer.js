import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { reducer as AuthReducer } from 'containers/Auth/slices'
import { reducer as ProfileReducer } from 'containers/Profile/slices'
import { reducer as PostReducer } from 'containers/Home/slices'

import history from 'utils/history'

export default function createReducer(injectedReducers = {}) {
	const appReducer = combineReducers({
		router: connectRouter(history),
		auth: AuthReducer,
		profile: ProfileReducer,
		posts: PostReducer,
		...injectedReducers,
	})

	const rootReducer = (state, action) => {
		return appReducer(state, action)
	}

	return rootReducer
}
