// const dev = process.env.NODE_ENV !== 'production'

const defaultHost = {
	host: 'staging.sloudly.com',
	staticUrl: 'https://memeshome.com',
	apiUrl: 'https://memeshome.com',
	dev: true,
	prod: false,
}

const hosts = {
	localhost: defaultHost,
	'staging.memeshome.com': {
		host: 'staging.sloudly.com',
		staticUrl: 'https://staging.memeshome.com',
		apiUrl: 'https://staging.memeshome.com',
		dev: true,
		prod: false,
	},
	'memeshome.com': {
		host: 'memeshome.com',
		staticUrl: 'https://memeshome.com',
		apiUrl: 'https://memeshome.com',
		dev: false,
		prod: true,
	},
}

const currentHost = hosts[window.location.hostname]
	? hosts[window.location.hostname]
	: defaultHost

export default currentHost
